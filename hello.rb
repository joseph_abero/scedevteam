#longest sentence
#number of sentences that are palindromes
#which palindrome is the longest

def isAlpha(str)
  !str.match(/[^A-Za-z]/)
end

def isNumber(str)
	if chr.to_i != 0
	  puts "It is number,  yep"
	end
end

def isPunct(char)
	if char == "."
		true
		#print "false"
	elsif char == "'"
		true
		#print "false"
	elsif char == ","
		true
		#print "false"
	elsif char == "!"
		true
		#print "false"
	elsif char == "?"
		true
		#print "false"
	elsif char == '"'
		true
	elsif char == ";"
		true
	elsif char == ":"
		true
	else
		false
		#print "true"
	end
end

def isWhitespace(char)
	char == " "
end

def FinalOutput(longest_sentence, numOfPalindromes, longest_palindrome)
	puts "FINAL OUTPUT \n ____________"
	puts "Longest Sentence: #{longest_sentence} \n"
	puts "Number of Palindromes: #{numOfPalindromes} \n\n"
	puts "Longest Palindrome: #{longest_palindrome} \n"
end


#start of main code

#initialize variables
longest_sentence = ""
longest_length = 0
longest_palindrome = ""
longest_p_length = 0
save_size = 0
same = true
numOfPalindromes = 0

temp = ""
s_char = ""
q_char = ""
stack = []
queue = []

x = 0
y = 0


#open file and read each line
File.open("input.txt").each do |line|

	#store the line of text into a string
	temp = line.downcase

	#read each character of the string
	until y == temp.size - 1
		#will add character to stack and queue if character 
		#is NOT punctuation or whitespace
		if((!isPunct(temp[y]) && !isWhitespace(temp[y])))
			stack.push(temp[y])
			queue.unshift(temp[y])
		end
		y += 1
	end

	#checks the if the sentence is the longest sentence in the file
	save_size = stack.size
	if stack.size > longest_length
		longest_length = save_size
		longest_sentence = line
	end

	#pops each character from stack and queue 
	#then checks if each character is the same
	#if each character is same, then it is a palindrome
	until stack.size == 0 && queue.size == 0
		s_char = stack.pop
		q_char = queue.pop
		if(s_char != q_char)
			same = false
		end
	end

	#determines if the sentence was a palindrome
	if same == true
		numOfPalindromes += 1
		if save_size > longest_p_length
			longest_p_length = save_size
			longest_palindrome = line
		end
	end

	#reset variables
	same = true
	y = 0
end

FinalOutput(longest_sentence, numOfPalindromes, longest_palindrome)